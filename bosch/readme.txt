
    Factory: To To separate the responsbility of object creation from it's user
    Adapter To provide a known interface to an unknown object
    Decorator To extend an object
    Proxy To do pre and post processing anonymously
    Composite To treat sum of parts as a whole
    Chain of Responsibility To escalate conditionally, in a hierarchy
    Strategy To select algorithm at run-time
    Abstract Factory To choose one from multiple factories for same interface
    Iterator to traverse a datastructure without knowing the structure