#include <iostream>

using namespace std;

class Question
{
public:
    string description;
    string a;
    string b;
    string c;
    string answer;
};

class PrintableQuestion : Question
{
private:
    Question target;

public:
    PrintableQuestion(Question q)
    {
        target = q;
    }
    void print()
    {
        string option;
        cout << target.description << endl;
        cout << "A. " << target.a << endl;
        cout << "B. " << target.b << endl;
        cout << "C. " << target.c << endl;
        cout << "Answer: ";
        cin >> option;
        if (option == target.answer)
            cout << "Correct :-)" << endl;
        else
            cout << "Incorrect :-(" << endl;
    }
};

class Quiz
{
public:
    virtual Question fetch(string subject) = 0;
};

class GlarimyQuiz : public Quiz
{
public:
    Question fetch(string subject)
    {
        if (subject == "politics")
        {
            Question q = Question();
            q.description = "Glarimy Question: Capital of India?";
            q.a = "Kolkota";
            q.b = "New Delhi";
            q.c = "Mumbai";
            q.answer = "B";
            return q;
        }
        else
        {
            Question q = Question();
            q.description = "Mock Question: 1+2?";
            q.a = "1";
            q.b = "2";
            q.c = "3";
            q.answer = "C";
            return q;
        }
    }
};

class QuizFactory
{
public:
    static Quiz *getQuiz()
    {
        Quiz *quiz = new GlarimyQuiz();
        return quiz;
    }
};

int main()
{
    Quiz *quiz = QuizFactory::getQuiz();
    try
    {
        Question q = quiz->fetch("politics");
        PrintableQuestion pq = PrintableQuestion(q);
        pq.print();
    }
    catch (char const *exception)
    {
        cout << exception << endl;
    }
};