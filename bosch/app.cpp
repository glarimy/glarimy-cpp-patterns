#include <iostream>

using namespace std;

class Question
{
public:
    string description;
    string a;
    string b;
    string c;
    string answer;
};

class PrintableQuestion : Question
{
private:
    Question target;

public:
    PrintableQuestion(Question q)
    {
        target = q;
    }
    void print()
    {
        string option;
        cout << target.description << endl;
        cout << "A. " << target.a << endl;
        cout << "B. " << target.b << endl;
        cout << "C. " << target.c << endl;
        cout << "Answer: ";
        cin >> option;
        if (option == target.answer)
            cout << "Correct :-)" << endl;
        else
            cout << "Incorrect :-(" << endl;
    }
};

class Quiz
{
public:
    virtual Question fetch(string subject) = 0;
};

class GlarimyQuiz : public Quiz
{
public:
    Question fetch(string subject)
    {
        if (subject == "politics")
        {
            Question q = Question();
            q.description = "Glarimy Question: Capital of India?";
            q.a = "Kolkota";
            q.b = "New Delhi";
            q.c = "Mumbai";
            q.answer = "B";
            return q;
        }
        else
        {
            Question q = Question();
            q.description = "Mock Question: 1+2?";
            q.a = "1";
            q.b = "2";
            q.c = "3";
            q.answer = "C";
            return q;
        }
    }
};

class ValidationProxy : public Quiz
{
private:
    Quiz *target;

public:
    ValidationProxy(Quiz *target)
    {
        this->target = target;
    }
    Question fetch(string subject)
    {
        if (subject == "")
            throw "Invalid Subject";
        return target->fetch(subject);
    }
};

class LoggingProxy : public Quiz
{
private:
    Quiz *target;

public:
    LoggingProxy(Quiz *target)
    {
        this->target = target;
    }
    Question fetch(string subject)
    {
        cout << "fetching question for " << subject << endl;
        Question question = target->fetch(subject);
        cout << "fetched question " << question.description << endl;
        return question;
    }
};

class QuizFactory
{
public:
    static Quiz *getQuiz(bool logging, bool validation)
    {
        Quiz *quiz = new GlarimyQuiz();
        if (validation)
        {
            Quiz *validator = new ValidationProxy(quiz);
            if (logging)
                return new LoggingProxy(validator);
            else
                return validator;
        }
        else
        {
            if (logging)
                return new LoggingProxy(quiz);
            else
                return quiz;
        }
    }
};

int main()
{
    string option;
    Quiz *quiz = QuizFactory::getQuiz(true, true);
    try
    {
        Question question = quiz->fetch("politics");
        PrintableQuestion pq = PrintableQuestion(question);
        pq.print();

    }
    catch (char const *exception)
    {
        cout << exception << endl;
    }
};